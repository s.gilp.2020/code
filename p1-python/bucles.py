#!/usr/bin/python3

amigos: list = ['ana', 'jacinto', 'guillermo']
print("Mis amigos:")
for invitado in amigos:
    print(invitado, len(invitado))

print("Otra vez mis amigos:")
for invitado in amigos:
    print(invitado, end=': ')
    for letra in invitado:
        print(letra, end='.')
    print()
