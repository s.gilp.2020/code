#!/usr/bin/python3
# -*- coding: utf-8 -*-

def sum(sumando1, sumando2):
    """Sums two integer/floats

    Returns integer/float."""

    return sumando1 + sumando2

if __name__ == "__main__":
    primero = int(input("Please enter an integer/float: "))
    segundo = int(input("Please enter another integer/float: "))
    print(sum(primero, segundo))
