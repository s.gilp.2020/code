#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Tests for simple calculator operations"""

import calc
import unittest

class TestAdd (unittest.TestCase):
    def test_integers(self):
        """
        Test the addition of two integers
        """
        result = calc.add(1, 2)
        self.assertEqual(result, 3)

    def test_floats(self):
        """
        Test the addition of two floats
        """
        result = calc.add(1.0, 2.0)
        self.assertEqual(result, 3.0)

class TestMul (unittest.TestCase):
    def test_integers(self):
        """
        Test the multiplication of two integers
        """
        result = calc.mul(1, 2)
        self.assertEqual(result, 2)

    def test_floats(self):
        """
        Test the multiplication of two floats
        """
        result = calc.mul(1.0, 2.0)
        self.assertEqual(result, 2.0)

if __name__ == '__main__':
    unittest.main()
