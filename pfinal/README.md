# Protocolos para la transmisión de audio y video en Internet
# Proyecto final (enero 2023)

**Conocimientos previos necesarios:**

* Programación Python (practicas previas)
* Documentos XML y JSON (clases de teoria, prácticas previas)
* Uso de Wireshark (prácticas previas)
* Nociones de SIP (clases de teoría, prácticas previas)
* Nociones de RTP (clases de teoría, prácticas previas)

**Tiempo estimado:** 25 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-pfinal

**Fecha de entrega (parte básica y adicional):** 15 de enero de 2023, 23:59

<!--
**Video de presentacion de detalles:** [Video grabado el 22 de diciembre](https://urjc.sharepoint.com/:v:/s/gr_2021.2039_2039021_3AM/EaSKvs8lC1RKuzA-9Ddn52YBWlbyZxHz8O1TgTuscJOWKw?e=8zYXLa) (requiere autenticación).
-->

## Introducción

Este proyecto tiene como objetivo implementar un servicio de recepción de canales de audio utilizando tecnología de VoIP (SIP y RTP, fundamentalmente). Para realizar el proyecto, el alumno tendrá que realizar tres programas:

* ServidorSIP. Un programa que actuará como _registrar_ y _redirector_. Como registrar, aceptará registro de direcciones SIP de servicios. Como redirector aceptará peticiones de inicio de sesión de clientes, respondiendo con redirecciones a las direcciones de los servidores correspondientes.
* ServidorRTP. Un programa que implementará el servicio de emisión de canales de audio. El programa se registrará en el _registrar_ con una dirección SIP de servicio, y si un cliente establece una sesión (llamada) cón él (usando SIP), le enviará mediante RTP un fichero de audio.
* Cliente. Un programa que implementará el cliente. El programa establecerá una sesión (llamada) con uno de los servicios, para recibir el fichero de audio correspondiente. Para ello, comenzará enviando una petición de inicio de sesión a ServidorSIP, especificando la dirección de servicio. ServidorSIP le contestará con una redirección a la dirección "real" del servidor, y Cliente, usándola, establecerá una sesión directamente con éste.

En este esquema, tanto ServidorRTP como Cliente funcionan como UAs SIP, aunque solo Cliente iniciará sesiones (enviará invitaciones) y solo ServidorRTP se registrará con su dirección de servicio y enviará datos multimedia (como paquetes RTP), que recibirá Cliente.

Por lo tanto, cada ServidorRTP tendrá una "dirección de servicio", genérica, y una "dirección real", que será la que se podrá usar directamente para conectarse con él. Cada ServidorRTP registrará con ServidorSIP su dirección de servicio, indicando que corresponde con su dirección real.

La forma normal de funcionar de los tres programas será la siguiente:

* Se lanza un ServidorSIP.

* Se lanza al menos un ServidorRTP, que se registrará en el ServidorSIP con su dirección SIP de servicio, y quedará esperando invitaciones.

* Se lanza al menos un Cliente, que iniciará una sesión, enviando una invitación al ServidorSIP indicando la dirección SIP de servicio de un ServidorRTP. La invitación llevará un documento SDP indicando los parámetros de la sesión.

* El ServidorSIP, actuando como _redirector_, enviará un mensaje de redirección al Cliente, indicando la direcciñon real que corresponde con la dirección de servicio.

* El Cliente contestará un ACK al ServidorSIP.

* El cliente enviará una invitación a la dirección real del ServidorRTP. La invitación llevará un documento SDP indicando los parámetros de la sesión.

* Si todo es correcto, el ServidorRTP responderá a la invitación aceptándola, para lo que enviará una respuesta OK al Cliente.

* El Cliente responderá con un ACK, que enviará a ServidorRTP. Tras hacerlo, quedará escuchando en el puerto donde espera recibir los paquetes RTP.

* Al recibir el ACK, el ServidorRTP comenzará a enviar los paquetes RTP correspndientes del fichero de audio que sirve directamente al Cliente, según los parámetros especificados en el documento SDP.

* Al cabo de un cierto tiempo el Cliente enviará un BYE a la dirección real del ServidorRTP. Al recibirlo, éste interrumpirá la transmisión de paquetes RTP y responderá on un OK, que enviará al Cliente.

* Al recibir el OK, el Cliente terminará, salvando previamente un fichero con el audio que ha recibido de ServidorRTP.

## Objetivos del proyecto

* Realizar un sistema SIP razonablemente realista.
* Poner en práctica lo aprendido en las prácticas anteriores.
* Probar la interacción de SIP y RTP en un entorno concreto.

## Puntuación

Este documento describe el proyecto final de la asignatura (práctica final), que puede aportar hasta 2.75 puntos (sobre un total de 10) en la nota final de la asignatura. Esta puntuación se organiza como sigue:

* El funcionamiento correcto de la parte básica del proyecto supondrá que el proyecto final se considera como aprobado, pero aportará 0 puntos a la nota final de la asignatura. Tener esta parte funcionando correctamente es requisito necesario para poder aprobar la asignatura.
* El funcionamiento correcto de la parte adicional del proyecto podrá suponer hasta 2 puntos de la nota final de la asignatura.
* Además, habrá una parte a desarrollar colaborativamente, donde se probará la interacción entre prácticas de distintos alumnos, que podrá suponer hasta 0.75 puntos de la nota final de la asignatura.

## Comienzo

Con el navegador, dirígete al repositorio plantilla de este proyecto y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se exponen a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la ETSIT).

## Cliente (parte básica)

El cliente ha de ejecutarse así:

```shell
$ python3 client.py <IPServidorSIP>:<puertoServidorSIP> <dirCliente> <dirServidorRTP> <tiempo> <fichero>
```

donde:

* `<IPServidorSIP>` es la dirección IP de la máquina donde está ServidorSIP
* `<puertoServidorSIP>` es el puerto UDP donde está escuchando ServidorSIP
* `<dirCliente>` es la dirección SIP del cliente, con la que se va a registrar en ServidorSIP.
* `<dirServidorRTP>` es la dirección SIP de servicio del servidor cn el que se va a iniciar una sesión para que nos envíe audio, tal y como la haya registrado el servidor en cuestión. Las direcciones tendrán que ser direcciones SIP válidas, por ejemplo `sip:heyjude@signasong.net`
* `<tiempo>` será el tiempo, en segundos, que el cliente mantendrá abierta la sesión una vez ésta haya empezado: al terminar este tiempo, el cliente enviará un `BYE` para terminar la sesión
* `<fichero>` será el fichero donde se guardará el audio recibido de ServidorRTP

Por ejemplo:

```shell
$ python3 client.py 127.0.0.1:6001 sip:yo@clientes.net sip:heyjude@songs.net 10 miaudio.mp3
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>
```

Al arrancar, el cliente se preparaá para recibir paquetes RTP, y enviará `INVITE` al ServidorSIP, indicando en un documento SDP el puerto UDP en que se ha preparado para escuchar RTP. Este documento SDP tendrá los siguientes elementos:

* `v`: versión, por defecto 0
* `o`: originador e identificador de sesión (la dirección SIP del Cliente y su dirección IP,
* `s`: nombre de la sesión, que será el nombre del usuario genérico SIP con el que se comunica
* `t`: tiempo que la sesión lleva activa, en nuestro caso, siempre 0
* `m`: tipo de elemento multimedia y puerto de escucha y protocolo de transporte utilizados, en esta práctica `audio`, el número de puerto pasado al programa principal como parámetro y ``RTP'').

Por ejemplo (suponiendo que el puerto 34543 es donde Cliente espera recibir los paquetes RTP):

```
INVITE sip:heyjude@songs.net SIP/2.0
Content-Type: application/sdp

v=0
o=sip:yo@clientes.net 127.0.0.1
s=heyjude
t=0
m=audio 34543 RTP
```

El Cliente quedará esperando la respuesta de ServidorSIP, que será en general un REDIRECT. Si efectivamente recibe un REDIRECT, el Cliente responderá al ServidorSIP con un ACK:

```
ACK sip:heyjude@songs.net SIP/2.0\r\n\r\n
```

A continuación, enviará de nuevo un INVITE como el anterior, pero ahora a la dirección IP y puerto indicados en la cabecera Contact del REDIRECT recibido, y usando la dirección "real" recibida en esa misma cabecera. Así, si la cabecera Contact recibida en el REDIRECT fue:

```
Contact: sip:heyjude@192.168.10.16:53001
```

El INVITE se enviará a la dirección IP 192.168.10.16, puerto 5001, y será como sigue:

```
INVITE sip:heyjude@192.168.10.16:53001 SIP/2.0
Content-Type: application/sdp

v=0
o=sip:yo@clientes.net 127.0.0.1
s=heyjude
t=0
m=audio 34543 RTP
```

El ciente quedará esperando el OK de ServidorRTP, y cuando lo reciba, enviará un ACK a ServidorSIP:

```
ACK sip:heyjude@@192.168.10.16:53001 SIP/2.0\r\n\r\n
```

Cuando haya pasado el tiempo especificado, Cliente enviará un BYE:

```
BYE sip:heyjude@s@192.168.10.16:53001 SIP/2.0\r\n\r\n
```

y cuando reciba el OK correspondiente, terminará, asegurándose antes de que se almacenan en el fichero especificado todas las muestras de sonido recibidas en paquetes RTP.

Para recibir los paquetes RTP desde `ServidorRTP` se puede usar un código similar al que se puede ver en el programa `recv_rtp.py` que hemos incluido en el repositorio plantilla de la práctica final. Ese código genera un fichero con los datos recibidos en los paquetes RTP, pero no necesariamente un fichero MP3 correcto. No hace falta que el fichero generado sea un fichero MP3 correcto,
basta con que se escriban en él los datos recibidos en el `payload` de los
paquetes RTP, en cualquier orden (que es lo que hace `recv_rtp.py`).
Revisa el código de este programa ejemplo con cuidado, los comentarios
deberían aclarar todos sus detalles.

## ServidorRTP (parte básica)

El servidor RTP ha de ejecutarse así:

```shell
$ python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio> <fichero>
```

donde:

* `<IPServidorSIP>` es la dirección IP de la máquina donde está ServidorSIP
* `<puertoServidorSIP>` es el puerto UDP donde está escuchando ServidorSIP
* `<servicio>` es el usuario que se uará como parte de usuario tanto en la dirección de servicio SIP como en la dirección real SIP para este servidor.
* `<fichero>` será el fichero con el sonido, que se enviará mediante RTP cuando se establezca una sesión con un cliente

Por ejemplo:

```shell
$ python3 serverrtp.py 127.0.0.1:6001 heyjude cancion.mp3
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>
```

Al arrancar, ServidorRTP enviará un `REGISTER` a ServidorSIP, para registrarse, utilizando como dirección SIP el nombre de usuario que se indicó como `<servicio>`, concatenado con `@songs.net`. Por ejemplo, si el usuario es `heyjude`, la dirección será `sip:heyjude@songs.net`. La petición por tanto será como esta:

```
REGISTER sip:heyjude@songs.net SIP/2.0\r\n\r\n
```

ServidorSIP deberá responder con un mensaje OK.

A continuación, ServidorRTP quedará esperando peticiones `INVITE` de los clientes. Cuando las reciba correctamente, responderá con un OK:

```
SIP/2.0 200 OK\r\n\r\n
```

Y empezará a enviar paquetes RTP al cliente, según los datos que haya podido obtener del documento SDP que viene en el `INVITE` que ha recibido.

Quedará también esperando un paquete `BYE` del cliente. Cuando le llegue, interrumpirá el envío de paquetes RTP a Cliente, y responderá con un OK:

```
SIP/2.0 200 OK\r\n\r\n
```

A partir de ese momento, quedará esperando una nueva petición `INVITE`  de otro cliente.

Para enviar los paquetes RTP al cliente, se puede usar un código similar al que se puede ver en el programa `send_rtp.py` que hemos incluido en el repositorio plantilla de la práctica final. 
Revisa el código de este programa ejemplo con cuidado, los comentarios
deberían aclarar todos sus detalles.


## ServidorSIP (parte básica)

El servidor SIP ha de ejecutarse así:

```shell
$ python3 serversip.py <puerto>
```

donde:

* `<puerto>` será el puerto UDP donde recibirá peticiones SIP tanto de clientes como de servidores RTP.

Por ejemplo:

```shell
$ python3 serversip.py 6001
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverrtp.py <port>
```

Al arrancar, ServidorSIP funcionará como _registrar_ y como _redirector_. Por lo tanto, aceptará varias peticiones SIP:

* `REGISTER`: Registrará en un diccionario que va a mantener con todas las direcciones registradas. El diccionario tendrá como claves las direcciones SIP que se registren, y como valor para cada clave las tuplas <IP>:<puerto> (dirección IP y puerto de origen del paquete de registro). En esas direcciones será donde los ServidorRTPs estarán esperando mensajes SIP. Los datos de este diccionario serán respaldados en el fichero JSON llamado `registrar.json`, con la misma estructura que el diccionario. Este fichero se actualizará cada vez que haya algún cambo en los datos de registro. Cuando arranque el servidor comprobará si existe un fichero con ese nombre, y si es así lo tratará de leer para inicializar el diccionario de registro.

* `INVITE`, `ACK`: Cuando los reciba, actuara como _redirector_. Por lo tanto, cuando reciba `INVITE` responderá con un paquete `REDIRECT`, indicando al dirección real que corresponde con la dirección de servicio que venía en el `INVITE`. Cuando reciba un `ACK`, no hará nada.

Por ejemplo, si se recibe un `REGISTER` ha sido enviado desde un ServidorRTP que está en la máquina 192.168.10.16, escuchando en el puerto 53001, y tiene un contenido como el del paquete `REGISTER` que envía el ServidorRTP en el ejemplo anterior, el ServidorSIP guardará en su diccionario de direcciones registradas una nueva entrada con la clave `sip:heyjude@songs.net`, y el valor `sip:heyjude@192.168.10.16:53001` (dirección "real" que corresponde a esa dirección de servicio). A continuación responderá con un paquete OK:

```
SIP/2.0 200 OK\r\n\r\n
```

Por ejemplo, si se recibe un `INVITE` como primer paquete que envió un Cliente en el ejemplo que se puso más arriba, la respuesta será una redirección `302 Moved Temporarily`, indicando en la cabecera `Contact` la dirección "real" con la que tendrá que contactar el cliente:

```
SIP/2.0 302 Moved Temporarily\r\n
Contact: sip:heyjude@192.168.10.16:5001\r\n
\r\n
```

## Históricos  (parte básica)

Todos los programas (Cliente, ServidorRTP y ServidorSIP) escribirán en su salida estándar (`stdout`) mensajes históricos (_log_) cuando ocurran ciertos eventos, siempre en el siguiente formato (nótese el punto final):

```
YYYYMMDDHHMMSS Evento.
```

donde `YYYYMMDDHHMMSS` es el tiempo, en ese formato, y `Evento` es:

* Cuando un programa empieza: `Starting...`

* Para cada envío/recepción de una petición o una respuesta SIP: `SIP to/from <ip>:<port> <line>.`, siendo `<ip>` y `<port>` la dirección IP y el puerto de destino o de origen de la petición o la respuesta SIP.

* Cuando un cliente se prepare para recibir paquetes RTP en un puerto: `RTP ready <port>`, siendo `<port>` ese puerto.

* Cuando un ServidorRTP empiece a enviar paquetes RTP a una cierta dirección IP y puerto: `RTP to <ip>:<port>`, siendo `<ip>` y `<port>` esa dirección y ese puerto.

Nótese, por tanto, que cada línea ha de contener la fecha en el formato indicado, luego un espacio en blanco y luego el texto del evento. Por ejemplo:

```
20211128152045 Starting...
20211128153012 SIP to 127.0.0.1:5555: REGISTER sip:heyjude@signasong.net SIP/2.0.
20211128153017 SIP from 127.0.0.1:5555: SIP/2.0 200 OK.
20211128153036 RTP to 127.0.0.1:4444.
```

## Capturas

Han de realizarse las siguientes capturas con Wireshark, que se entregarán con el resto del proyecto en el repositorio correspondiente:

* `capture.libpcap`: Paquetes SIP y RTP intercambiados entre todos los programas en una transferencia de un audio desde un ServerRTP a un Cliente, durante 10 segundos. Se realizará lanzando, en este orden (en consolas diferentes) los siguientes programas con los correspondientes argumentos:

```shell
$ python3 serversip.py 6001
$ python3 serverrtp.py 127.0.0.1:6001 heyjude cancion.mp3
$ python3 client.py 127.0.0.1:6001 sip:yo@clientes.net sip:heyjude@songs.net 10 received.mp3
```

Además del fichero con la captura, se incluirá también el fichero `received.mp3`.

* `capture2.libpcap`: Paquetes SIP y RTP intercambiados entre todos los programas en una transferencia de un audio desde un ServerRTP a dos clientes, en secuencia, durante 10 segundos con cada uno. Se realizará lanzando, en este orden (en consolas diferentes) los siguientes programas con los correspondientes argumentos:

```shell
$ python3 serversip.py 6001
$ python3 serverrtp.py 127.0.0.1:6001 heyjude cancion.mp3
$ python3 client.py 127.0.0.1:6001 sip:yoa@clientes.net sip:heyjude@songs.net 10 received2a.mp3
$ python3 client.py 127.0.0.1:6001 sip:yob@clientes.net sip:heyjude@songs.net 10 received2b.mp3
```

Además del fichero con la captura, se incluirán también los ficheros `received2a.mp3` y `received2b.mp3`.


## Peticiones concurrentes (parte adicional)

ServidorRTP acepta peticiones concurrentes de varios clientes

## Configuración XML (parte adicional)

ServidorRTP utiliza un fichero XML para almacenar datos sobre direcciones de servicio SIP y nombres de ficheros, de forma que un solo servidor puede servir un conjunto de direcciones, cada una correspondiente con un fichero diferente

## Envío por cliente (parte adicional)

Cliente envía en cada sesión que establezca con ServidorRTP un mensaje de audio. ServidorRTP almacenará todos los ficheros de audio que reciba de los clientes

## Cabecera de tamaño (parte adicional)

Se incluirá en los paquetes SIP la cabecera de tamaño del cuerpo del paquete, cuando haya cuerpo.

## Tiempo de expiracion (parte adicional)

Tiempo de expiración de las direcciones SIP registradas, utilizando la cabecera correspondiente en `REGISTER`. Los clientes se registrarán por el tiempo que se especifique al llamarlos. Los ServidorRTP se registrarán por 62 segundos, y al cabo de 30 segundos renovará el registro enviando un nuevo `REGISTER`, de nuevo con periodo de validez de 62 segundos (así el registro se mantendrá aunque se perdiera un paquete `REGISTER`).

## Gestión de errores (parte adicional)

Gestión de errores, enviando las correspondientes respuestas en lugar de `200 OK`.

## Entrega de la práctica

La práctica se entregará en el repositorio del alumno en el GitLab de la ETSIT, que habrá sido derivado (haciendo "fork", como se indica al comienzo de este enunciado) del repositorio plantilla del proyecto final.

El repositorio deberá incluir los ficheros que había en el repositorio plantilla, más:

* Ficheros con programas principales `serversip.py`, `serverrtp.py` y `client.py`.
* Capturas indicadas en el apartado "Capturas", más arriba: `capture.libpcap` y `capture2.libcap`

* Fichero de entrega, `entrega.md`, en formato Markdown, con la siguiente información:

  - Sección sobre la parte básica (`## Parte básica`). Incluirá cualquier comentario relevante sobre la parte básica de la práctica, incluyendo cualquier aspecto que no funcione como se indica en el enunciado.
  - Sección sobre la parte adicional (`## Parte adicional`). Incluirá cualquier comentario relevante sobre la parte adicional de la práctica, incluyendo fundamentalmente el listado de las opciones que se han realizado (una por línea, comenzando por `* `, para que queden en un listado, y con el mismo contenido en esa línea que el nombre del apartado correspondiente paa esa opción). Por ejemplo:

```markdown
* Configuración XML

    Explicación
  
* Cabecera de tamaño

    Explicación
```

  Además de este listado incluirá una explicación (ver el ejemplo anterior) sobre cómo probar esa parte opcional, indicando al menos qué programas hay que lanzar, con qué argumentos, y cómo se puede comprobar que funciona el apartado en cuestion.

  Si se ha realizado cualquier parte adicional que no venga indicada específicamente en el enunciado (por ejemplo, se han implementado peticiones SIP adicionales, o cabeceras SIP adicionales), puede indicarse en este listado como `Otra:`, seguido de una descripción de la mejora (una entrada `Otra` por cada mejora).

  - Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Es importante comprobar que este documento se ve correctamente desde la interfaz eb de GitLab (eso es, que el marcado Markdown se interpreta correctamente)

## Preguntas y respuestas

### ¿Cómo puedo actualizar mi repositorio se actualiza el repositorio plantilla de la práctica?

Puede ocurrir que mientras estás trabajando con tu práctica, después de haber hecho tu fork para poder trabajar clonándolo localmente, actualicemos el repositorio plantilla (del que hiciste el fork). Puedes incorporar las modificaciones al repositorio plantilla en el tuyo, entre otrsas, de las dos maneras siguientes.

Por un lado, puedes utilizar la interfaz web de GitLab para descargar los nuevos ficheros, o las nuevas versiones de los ficheros que se hayan modificado, y copiarlos en tu repositorio (añadiéndolos después con `git add` e introduciéndolos en un commit, si eso es conveniente).

Por otro lado, puedes utilizar git. Para ello, primero, tienes que añadir, a tu repositorio local (el que clonaste de tu fork) un nuevo `remote`. Los "remote" son los repositorios remotos con los que tu repositorio local puede sincronizarse. Tu repositorio tiene un remote (llamado `origin`) que corresponde con tu repositorio en GitLab (el que creaste como fork del repositorio plantilla). Puedes ver los remotes de tu repositorio:

```shell
git remote -v
```

Verás dos líneas para `origin`, una para `fetch` (recibir datos) y otra para `push` (enviar datos).

Para poder actualizar a partir del repositorio plantilla, vamos a añadir un nuevo remote para él:

```shell
git remote add upstream https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-pfinal-21
```

Si ahora vuelves a ejecutar `git remote -v` verás cuatro líneas, dos para el remote `origin` y dos para el nuevo remote `upstream`.

En este momento es importante comprobar si tienes pendientes modificaciones a ficheros que conoce git (esto es, hay modificaciones que aún no han sido incluidas en ningún commit). Para ello, puedes comprobar que `git status` está limpio, o puedes tratar de realizar un commit que incluya todos los cambios pendientes (si no hay cambios git te lo dirá). Para tratar de haer este commit puedes escribir, en el directorio principal de tu repositorio (atención al `.` final):

```shell
git commit .
```

Ahora que ya tenemos al repositorio plantilla como remote, y estamos seguros de que no hay cambios pendientes, podemos sincronizar con él. Si no has tocado los ficheros que obtuviste del repositorio, git se encargará de gestionar la sincronización completamente. Si los has tocado, puede ser un poco más complicado, pero posible. Por si acaso, antes de realizar los siguientes pasos, copia los ficheros que hayas escrito (tus programas, fundamentalmente) a otro directorio, por si acaso.

La forma más simple de sincronizar los contenidos del repositorio plantilla es la siguiente:

```shell
git fetch upstream
git rebase upstream/master
```

La primera orden bajará la informacion nueva que pueda haber en el remote `upstream` (el reposito plantilla de la practica), pero no la incorporara en el directorio. La segunda orden incorporara los commits nuevos que pueda haber en la rama principal de `upstream` en la rama en la que estés trabajando en tu repo local. Para hacerlo, `git rebase` hará comprobaciones, para asegurarse de que esa incorporación es segura, sin perder información. Además, colocará los commits que tengas pendientes en tu rama detrás de los commits que descargue de `upstream/master`.

Si `git rebase` falla, porque no puede asegurar que la incorporación de commits es segura, te lo dirá, dándote varias opciones, ente ella la de abortar. Si es el caso, trata de entender la situacuón, y en caso de duda, aborta. Probablmente tengas cambios que den conflicto, o tengas cambios si incluir en un commit. Si `git rebase` tiene éxito, ya tienes en tu repositorio local los cambios que se hayan hecho en el repositorio plantilla de la práctica.

Alternativamente a `git rebase` puedes usar `git merge`, que hará algo muy parecido, pero manteniendo la historia de commits de otra forma:

```shell
git fetch upstream
git merge upstream/master
```

Si por algún motivo estás trabajando en una rama distinta de la principal, y quieres incorporar los cambios a la principal, asegúrate que pasas a esa rama principal antes, con `git checkout master`.

### ¿Cómo puedo enviar los paquetes RTP desde ServidorRTP a Cliente?

Utiliza los servicios que proporcion el paquete 'simplertp.py', que está incluido en el repositorio de plantilla de la práctica. Asegúrate de que estás usando la última versión, porque hemos incluido algunas mejoras que simplifican su uso. Puedes ver un ejemplo de cómo usarlo en el fichero 'send_rtp.py', también en el repositorio de plantilla de la práctica (lee sus comentarios, deberían explicar todos los detalles). Su uso se puede resumir en:

* Instancia un objeto de la clase 'simplertp.RTPSender', dándole como parámetros al dirección IP y el puerto UDP a los que enviar los paquetes RTP, y el nombre del fichero MP3 a enviar:

```python
sender = simplertp.RTPSender(ip=IP, port=PORT, file='cancion.mp3', printout=True)
```

* Usa ese objeto para enviar los paquetes. Usa el método que crea un hilo aparte para el envío (`send_threaded`), de forma que continúe la ejecución normal de tu programa principal mientras se está realizando el envío:

```python
sender.send_threaded()
```

### ¿Cómo puedo parar el envío RTP desde ServidorRTP cuando se recibe BYE?

Si has usado el método `send_threaded` para envío de RTP en su propio hilo (thread) que se menciona anteriormente, puedes parar el envío utilizando el método `finish` del objeto 'simplertp.RTPSender' que creaste:

```python
sender.finish()
```

Mira en el programa de ejemplo `send_rtp.py` para ver cómo se usa este método para interrumpir el envío RTP cuando han pasado unos segundos (en tu caso, lo tendrás que hacer cuando se reciba el BYE del cliente, pero la idea es la misma).

**Importante**: Asegúrate de que tienes la última versión de `simplertp.py` (la que está ahora mismo en el repositorio de plantilla de la práctica), porque si no, es posible que no tenga el método `finish` o que este no funcione adecuadamente.

### ¿Cómo puedo recibir los paquetes RTP en Cliente?

Los paquetes RTP que llegan al cliente son paquetes UDP. Por tanto, podemos recogerlos con un objeto `socketserver.UDPServer`, como puedes ver en el ejemplo `recv_rtp.py` que podrás encontrar en el repositorio plantilla de la práctica. Observa que en él hemos usado un manejador (handler), `RTPHandler`, heredado de la clase `socketserver.BaseRequestHandler`, y no de la clase `socketserver.DatagramRequestHandler` como hemos hecho en otras ocasiones. El motivo es que la segunda clase siempre emite un paquete UDP al terminar el método `handle`, con lo que se haya escrito en `self.wfile` (si no se ha escrito nada, enviará un paquete UDP vacio). Y en este caso, queremos que sólo se reciban paquetes RTP, no que se envíe nada como respuesta.

Por lo demás, lee los comentarios de `recv_rtp.py` para hacerte una idea más detallada. Lo principal es que para cada paquete UDP que se reciba, tenemos que recoger la carga (payload) del paquete RTP que va dentro de él (que en general vendrá a partir del byte 12 de los datos del paquete UDP), y escribirla en el fichero de salida donde Cliente tiene que almacenar el audio que llegue.

El fichero generado de esta forma es normalmente un fichero MP3 correcto, que se puede escuchar con cualquier aplicación que nos permita escuchar MP3. Pero hay que tener en cuenta que como los paquetes RTP van sobre UDP, pueden haber sufrido pérdidas y desórdenes, por lo que el fichero resultante podría no oirse como el fichero original, e incluso no oirse en determinadas circunstancias. Estas circunstancias son muy, muy raras si se está probando todo sobre localhost.

### ¿Cómo puedo tener un servidor UDP que escuche en un puerto cualquiera?

Hay varios casos en los que será conveniente crear un `UDPServer` que escuche en cualquier puerto disponible, y luego saber cuál es ese puerto. Esto ocurrirá, por ejemplo en:

* Cliente, para poder crear un receptor de RTP. No hay ningún argumento de línea de comandos para indicar en qué puerto el cliente va a recibir los paquetes RTP, así que habrá que abrir un `UDPServer` que escuche en un puerto libre cualquiera, y luego mediante SDP se indicará a ServidorRTP cuál es ese puerto para que envíe sus paquetes RTP.

* ServidorRTP, para poder crear un receptor de SIP. No hay tampoco ningún argumento de línea de comandos para indicar en qué puerto va a escuchar las peticiones SIP, así que habrá que abrir un `UDPServer` que escuche en un puerto libre cualquiera, y luego se le indicará a ServidorSIP cuando se haga `REGISTER`.

La forma de crear un `UDPServer` en ambos casos es la misma: indicando, como puerto en la tupla (IP, puerto) que hay que pasarle, el puerto 0. Este "puerto" tiene el significado especial de "elige cualquier puerto libre de la máquina". Igualmente, como dirección IP podemos usar "0.0.0.0" que quiere decir "escucha en todas las interfaces IP que tenga la máquina".

Así, el código será similar al siguiente:

```python
with socketserver.UDPServer(("0.0.0.0", 0), RTPHandler) as serv:
    rtp_port = serv.server_address[1]
```

La línea donde se asigna valor a `rtp_port` nos permite ver cómo podemos obtener el puerto en que ha quedado recibiendo nuestro objeto `UDPServer`.

En el caso de ServidorRTP, podemos usar un código como el siguiente para crear el `UDPServer`, registrarse (enviar la petición REGISTER) usando su socket, y luego lanzar ya el bucle de recepción de peticiones SIP, por si un cliente le envía alguna vía el ServidorSIP. Algo como:

```python
with socketserver.UDPServer(('0.0.0.0', 0), SIPHandler) as serv:
    # Registramos con el socket que usará el servidor SIP
    # register enviará REGISTER a ServidorSIP, y esperará respuesta
    register(serv.socket, client_addr, 3600, server_ip, server_port)
    # Ahora ya podemos activar el bucle para recibir peticiones SIP
    # en el mismo socket
    serv.serve_forever()
```

### ¿Cómo puedo dejar de recibir paquetes RTP en Cliente?

Cuando haya pasado el periodo de recepción que se especificó al invocar `cliente.py`, el programa debe enviar un BYE de SIP, y al recibir el OK correspondiente, debería dejar de recibir paquetes RTP (debería dejar de esperarlos). Para hacerlo, podemos lanzar el bucle de recepción del servidor UDP (`serve_forever`) en un hilo aparte (`serv` es el servidor UDP):

```python
threading.Thread(target=serv.serve_forever).start()
```

De esta manera el programa principal continuará después de lanzar ese hilo, en el que el bucle de recepción empezará a funcionar. Así, el programa principal puede hacer más cosas, como por ejemplo esperar el tiempo que sea necesario, enviar BYE a ServidorRTP (vía ServidorSIP) cuando toque, y terminar el bucle de recepción cuando le llegue el OK de respuesta. Para terminar el bucle de recepción podemos usar este código:

```python
serv.shutdown()
```

Esto terminará el bucle de recepción del servidor UDP, con lo que terminará tambien el hilo que creamos, pues no tiene nada que hacer cuando termine `serve_forever`.

Este código puede verse en el programa de ejemplo `recv_rtp.py`, donde hay un `time.sleep()` para simular con un retardo el tiempo que se estaría esperando a que pase el plazo, el envío de BYE y la posterior recepción de OK.


### ¿Por qué me sale un mensaje de warning al enviar paquetes RTP usando simplertp?

Si cuando envías paquetes RTP usando el paquete `simplertp` que hemos incluido en el repositorio plantilla de la práctica ves mensajes como

```
Warning: Connection refused. Probably there is nothing listening on the other end.
```

lo que muy probablemente está ocurriendo es justamente lo que dice: que se ha detectado que no hay nada escuchando los mensajes que se envían. El mensaje debería desaparecer cuando te esté funcionando el código de recepción de paquetes RTP. De hecho, si te sale este mensaje, es casi seguro que ese código no está funcionando, o no está escuchando en la dirección y puerto a los que estás enviando los paquetes RTP.

Lee más arriba cómo escribir el código para recibir los paquetes RTP.

### Las aplicaciones, ¿tienen que mostrar sólo los mensajes históricos que se indica?

Las aplicaciones deberían mostrar sólo los mensajes que se indican en el apartado correspondiente (Históricos). En principio, no deberían mostrarse más mensajes en al consola. Es importante que muestren, eso sí, todos los mensajes históricos que se indican en ese apartado.

Si crees que es necesario que se escriba algún mensaje más, documéntalo en el fichero de entrega, en el apartado de comentarios.

### ¿Puedo usar la clase `socketserver.DatagramRequestHandler` en lugar de `socketserver.BaseRequestHandler` para mis manejadores UDP?

En esta práctica se pide que uses la clase `socketserver.BaseRequestHandler` como base para tus manejadores UDP, como hecho en todas las prácticas anteriores.

### ServidorSIP: ¿qué peticiones SIP tiene que entender?

ServidorSIP tiene que entender al menos las siguientes peticiones SIP: REGISTER (porque funciona como un registrar SIP), INVITE y ACK (porque funciona como un redirector SIP).


### ServidorRTP: en la parte adicional "concurrentes", ¿cómo puedo saber qué cliente me está enviando un BYE, para terminar el envío correspondiente?

Si se implementa la parte adicional "peticiones concurrentes", puede ocurrir que un ServidorRTP reciba INVITE de dos clientes distintos, y empiece a enviar datos RTP a cada uno de ellos. ¿Cómo se puede saber, cuando llega una petición BYE a cuál de los dos clientes corresponde?

Hay varias formas en que se puede implementar esto, pero quizás la más sencilla pase por implmentar la cabecera SIP 'From' en las peticiones del cliente, con el siguiente formato:

```
From: <sip:yo@dir>;tag=3rffre34544nnre445
```

Esta es una cabecera correcta de SIP, que indica de dónde viene la petición. La dirección SIP es la del cliente, y la etiqueta (`tag`) es una cadena de texto aleatoria (que en este caso incluye dígitos y letras minúsculas), que es la misma sólo para la misma sesión (la secuencia INVITE, ACK y BYE para establecer y liberar una conersación).

Usando esta cabecera, ServidorRTP puede anotar en un diccionario, que podrá ser una variable de la clase de su clase manejadora ("handler"), a qué `From` corresponde cada emisor de RTP que inicie. Para ello puede usar como clave el contenido del campo `From` de la petición que lo inició, y como dato el objeto emisor RTP de `simplertp`).

Así, cuando llegue un BYE, tendrá tambié un campo `From`. Usando su valor en ese diccionario, podrá saber el objeto emisor RTP del que tiene que llamar `finish` para terminar la emisión.

Podemos generar un tag aleatorio fácilmente con un método `randomstr` como este:

```python
import secrets
import string
...
def randomstr():
   letters = string.ascii_lowercase
   return ''.join(secrets.choice(letters) for i in range(20))
```
