# Protocolos para la transmisión de audio y video en Internet
# Práctica 6. Sesión SIP entre iguales

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta un 0.7 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de SIP (las de clase de teoría)
* Funcionamiento de _Wireshark_ (prácticas anteriores)

**Tiempo estimado:** 10 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-p6

**Fecha de entrega parte individual:** 9 de diciembre de 2022, 23:59 (hasta ejercicio XX, incluido)

**Fecha de entrega parte interoperación:** 13 de diciembre de 2022, 23:59 (ejercicio YY)

## Introducción

Esta práctica tiene como objetivo implementar dos programas Python, de manera que se pueda realizar una sesión SIP como la que se muestra en la siguiente figura.

En la figura las respuestas SIP están en mayúsculas para mejorar su lectura; en tu implementación deberían seguir el estándar, en el que sólo la primera letra es mayúscula.

![Sesión SIP](sip-invite.png)

En esta sesión:

* No habrá ni servidor de registro ni proxy. La comunicación será _peer-to-peer_ (entre iguales, directa entre dos UAs, _user agents_). Un UA incluye normalmente una parte cliente y una parte servidora, aunque en esta práctica, por simplificar, cada UA tendrá sólo una de las dos partes.
* El UA que aparece en la figura en la izquierda constará exclusivamente de la parte cliente. Esta parte iniciará la sesión con un `INVITE` y la cerrará con un `BYE`.
* El UA que aparece a la derecha constará exclusivamente de la parte servidora Esta parte enviará el audio vía RTP al cliente.

## Objetivos de la práctica

* Implementación de una sesión de transmisión de audio unidireccional iniciada con SIP.

## Ejercicio 1. Creación de repositorio para la entrega

Con el navegador, dirígete al [repositorio plantilla de esta práctica] y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la ETSIT).

## Ejercicio 2. Cliente

El cliente ha de ejecutarse así:

```shell
$ python3 client.py <metodo> <receptor>@<IPreceptor>:<puertoSIP>
```

donde `<metodo>` será un método (petición) SIP, `<receptor>` será el identificador de usuario receptor, `<IPreceptor>` será al dirección IP del servidor, y `<puertoSIP>` será la dirección IP del puerto del servidor. En el mundo "real", el puerto sería el puerto de SIP por defecto, 5060, ya que normalmente sólo hay un UA activo por máquina. En nuestro caso va a ser conveniente poder probar con varios UA en la misma máquina, así que escribiremos nuestros programas de forma que el puerto en el que está la parte servidora el UA se pueda especificar.

Por ejemplo:

```shell
$ python3 client.py INVITE batman@193.147.73.20:5555
$ python3 client.py BYE batman@193.147.73.20:5555
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>
```

El cliente debería poder enviar las siguientes peticiones SIP:

* `INVITE sip:<receptor>@<IP> SIP/2.0`

Mediante este método, el UA indica que quiere iniciar una conversación con el receptor con dirección `<receptor>` en la máquina dada por la dirección `<IP>` (que puede ser 127.0.0.1).

* `ACK sip:<receptor>@<IP> SIP/2.0`

Método de asentimiento. Se enviará de manera automática una vez se hayan recibido del servidor la respuestas `200 OK`.

* `BYE sip:<receptor>@<IP> SIP/2.0`

Mediante este método se indica que queremos terminar una conversación con el receptor con dirección `<receptor>` en la máquina dada por la dirección `<IP>`. Se deberá enviar una vez haya acabado de recibir el flujo (_stream_)  de audio (vía RTP) enviado desde el servidor.

## Ejercicio 3. Servidor

El servidor ha de ejecutarse de la siguiente manera:

```shell
$ python3 server.py <IP> <puerto> <fichero_audio>
```

Por ejemplo:

```shell
$ python3 server.py 127.0.0.1 5555 cancion.mp3
```

En caso de no introducir el número de parámetros correctos o de error en los mismos (se ha de comprobar si existe el fichero de audio), el programa debería imprimir:

```shell
Usage: python3 server.py <IP> <port> <audio_file>
```

En caso de no haber error al arrancar, el servidor imprimirá por pantalla:

```shell
Listening...
```

El servidor enviará los siguientes códigos de respuesta:

* `SIP/2.0 200 OK`. En caso de éxito, al recibir un `INVITE`.

* `SIP/2.0 400 Bad Request` Si la petición está mal formada.

* `SIP/2.0 405 Method Not Allowed` Si se recibe del cliente cualquier otro método diferente de `INVITE`, `BYE` o `ACK`.


## Ejercicio 4. SDP en invitación

El cuerpo del `INVITE` debe incluir la descripción de sesión en formato SDP (Session Description Protocol). Contará con los siguientes parámetros:

* v: versión, por defecto `0`
* o: origen e identificador de sesión: la dirección del origen y su IP
* s: nombre de la sesión, que puede ser el que se desee
* t: tiempo que la sesión lleva activa, en nuestro caso, siempre `0`
* m: tipo de elemento multimedia y puerto de escucha y protocolo de transporte utilizados, en esta práctica `audio`, el número de puerto pasado al programa principal como parámetro y `RTP`

Así, un ejemplo de descripción de sesión dentro de un `INVITE` podría ser como se indica a continuación.

```
INVITE sip:batman@193.147.73.20 SIP/2.0

v=0
o=robin@gotham.com 127.0.0.1
s=misesion
t=0
m=audio 34543 RTP
```

El servidor guardará la dirección de origen y la dirección IP recibida por SDP en un diccionario. Este diccionario ha de ser un [atributo de clase](http://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide), no de la instancia, para utilizarlo posteriormente en el envío de RTP (porque cada vez que se recibe un mensaje se crea y luego se destruye una instancia de la clase).

Nótese que en el ejemplo anterior  `34543` es el puerto donde esperamos que el otro participante en la conversación nos envíe los paquetes RTP con audio. También se ha de tener en cuenta que entre las cabeceras (en este caso la línea de `INVITE`) y el cuerpo (la descripción de la sesión) ha de haber obligatoriamente una línea en blanco.

El dominio `gotham.com` es un dominio SIP arbitrario en el que suponemos que tienen lugar las comunicaciones. Nótese que, en esta práctica, es el que se usa en los documentos SDP para indicar direcciones "origen" completas, tanto del cliente como del servidor: las que van en el campo `o` del documento SDP.

## Ejercicio 5. SDP en respuesta

El cuerpo de la respuesta `200 OK` al `INVITE` debe incluir la descripción de sesión en formato SDP (Session Description Protocol). Contará con los mismos parámetros que se describen en el ejercicio 4 (SIP en invitación).

Así, un ejemplo de descripción de sesión en la respuesta a un INVITE podría ser:

```
SIP/2.0 200 OK

v=0
o=batman@gotham.com 127.0.0.1
s=misesion
t=0
m=audio 67876 RTP
```

Se ha de tener en cuenta que entre las cabeceras (en este caso la línea de `200 OK`) y el cuerpo (la descripción de la sesión) ha de haber obligatoriamente una línea en blanco. Nótese que como el UA que inicia la conversación no envía paquetes RTP, la información en este documento SDP no se utilizará en realidad para enviar paquetes RTP, y será simplemente ignorada.


## Ejercicio 6. Cabecera de contenido SDP

Se ha de añadir la cabecera `Content-Type: application/sdp` a todos los paquetes SIP que contengan SDP.

Así, un ejemplo de descripción de sesión dentro de un `INVITE` podría ser:

```
INVITE sip:batman@193.147.73.20 SIP/2.0
Content-Type: application/sdp

v=0
o=robin@gotham.com 127.0.0.1
s=misesion
t=0
m=audio 34543 RTP
```


## Ejercicio 7. Cabecera de tamaño

Se ha de añadir la cabecera `Content-Length`, cuyo valor será la longitud (en bytes) del cuerpo del paquete, a todos los paquetes SIP que contengan SDP.

Así, un ejemplo de descripción de sesión dentro de un `INVITE` podría ser:

```
INVITE sip:batman@193.147.73.20 SIP/2.0
Content-Type: application/sdp
Content-Length: 66

v=0
o=robin@gotham.com 127.0.0.1
s=misesion
t=0
m=audio 34543 RTP
```


## Ejercicio 8. Envío RTP

Este ejercicio consiste en implementar el envío, por parte del servidor, del fichero de audio que se especifica en la línea de comandos al lanzarlo. El envío sólo se realizará después de recibir el `INIVITE` correcto de un cliente, y de haberle respondido con un `200 OK`.

El envío RTP se realizará mediante el módulo `simplertp`. Para poder usarla, en el repositorio de plantilla de la práctica se encuentra el módulo Python `simplertp.py`. Un ejemplo del código Python de envío de paquetes RTP sería el siguiente:

```python
import simplertp

RTP_header = simplertp.RtpHeader()
RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
audio = simplertp.RtpPayloadMp3(audio_file)
simplertp.send_rtp_packet(RTP_header, audio, ip, port)
```

Donde `ALEAT` será un número entero aleatorio obtenido utlizando la [biblioteca `random` de Python3](https://docs.python.org/3.8/library/random.html).

La dirección IP y el puerto a los que se ha de enviar el flujo RTP vendrán dados en el documento SDP que se recibió del cliente (ver ejercicios anteriores).

Nótese que `audio_file` es uno de los parámetros que se pasa como parámetro al servidor al ejecutarse desde la línea de shell. También ha de notarse que si sale el aviso _Warning: Connection refused. Probably there is nothing listening on the other end_ es porque no hay ningún servicio escuchando al otro lado (en el lado del cliente). Esto no es en si mismo un error: indica que los paquetes han llegado al destino final, pero allí no han sido recibidos por nadie (porque el cliente aún no implementa la funcionalidad de recibir los paquetes RTP).


## Ejercicio 9. Captura

Una vez terminados los apartados anteriores, se pide que se realice la captura de un establecimiento de llamada, el envío RTP de audio y la finalización de
la llamada. 

La captura original se filtrará para que sólo incluya los paquetes SIP y los tres primeros y tres últimos paquetes RTP con audio. Los heurísticos de Wireshark puede que no identifiquen los paquetes RTP como tales, sino como paquetes UDP. Para evitar esto, en el menú de `Analyze`, selecciona `Enabled protocols` y ahí busca RTP para activar `rtp_udp`. Es recomendable también que deactives `Skype`, si estuviera activado, pues hay paquetes SIP que son erróneamente identificados como paquetes Skype.

La captura filtrada resultante se guardará en el fichero `invite.libpcap`
y se subirá al repositorio.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Para la corrección de la práctica, se espera que el alumno haya aportado 2 ficheros Python y la captura realizada con Wireshark:

* `server.py`
* `client.py`
* `invite.libpcap`

Además, ten en cuenta:

* Se valorará que haya realizado al menos haya ocho commits, correspondientes más o menos con los ejercicios pedidos, en al menos dos días diferentes, sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8 y PEP257.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorará que los programas se invoquen exactamente según se especifica, y que muestren  mensajes y errores correctamente según se indica en el enunciado de la práctica.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.
* En esta práctica se va a realizar una entrevista personal en todos los casos en que haya dudas en la corrección, y a un grupo de alumnos elegidos aleatoriamente. En esta entrevista se preguntará por cualquier detalle de la implementación de la práctica, y se espera que el alumno que la haya realizado pueda comentar ese detalle con conocimiento de causa.

## ¿Cómo puedo probar esta práctica?

Para muchos de los apartados del programa, se proporciona un test. Puedes ejecutarlo para ver que el test pasa. Ten en cuenta que el hecho de que pase el test no quiere decir que la parte correspondiente esté completamente bien, aunque los tests están diseñados para probar los errores más habituales. Recuerda que si el test es `test_xxx.py`, para ejecutarlo podrás ejecutarlo en PyCharm, o desde la línea de comandos:

```shell
python3 -m unittest tests/test_xxx.py
```

Cuando tengas la práctica lista, puedes realizar una prueba general, incluyendo la comprobación del estilo de acuerdo a PEP8, que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Ejercicio 10 (segundo periodo)

Elije el repositorio de uno de tus compañeros que hayan entregado la práctica. Ejecuta tu cliente con su servidor, y realiza una captura como la que se indica en el ejercicio 9. Guárdala en un fichero llamado `invite-<user>.libpcap`, y añádela a tu repositorio de entrega. `<user>` será el nombre de usuario de tu compañero en el GitLab de la ETSIT.