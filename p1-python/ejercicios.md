# Protocolos para la transmisión de audio y video en Internet
# Práctica 1. Introducción a Python

**Objetivos:** Conocer el funcionamiento básico del lenguaje Python, en particular: naturaleza del lenguaje, 
sintaxis, tipos abstractos de datos (listas, diccionarios y tuplas), gestión de ficheros y excepciones.

**Conocimientos teóricos previos:** Conocimientos básicos en algún lenguaje de programación orientado a 
objetos, preferiblemente Java, ADA o Pascal.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 2 horas (1 sesión de 
prácticas en el laboratorio).

**Fecha de entrega de la pŕactica:** Esta práctica NO requiere ser entregada

## Introducción

Python es un lenguaje interpretado, por lo que no hace falta que sea compilado para 
poder obtener un ejecutable. Los ejercicios se complementan con las transparencias presentadas 
por el profesor de prácticas en el laboratorio.

## Ejercicio 1

Realiza las siguientes actividades en el intérprete de Python.

* Invoca el intérprete de Python desde la shell (`python3`). 

* Crea las siguientes variables: 
  * un entero,
  * un real, 
  * una cadena de caracteres con tu nombre, 
  * una lista de cadenas de caracteres con los acrónimos de las asignaturas de este cuatrimestre

* Imprime por pantalla la longitud de la cadena de caracteres y de la lista.

* Muestra con `print` el tercer elemento de la lista
y el tercer caracter de tu nombre.

* Al finalizar este ejercicio, sal del intérprete de pyton con Ctrl+D 
o `exit()`para volver a la shell (intérprete de comandos).

## Ejercicio 2

Crea un script (pequeño programa) que haga lo siguiente:

* Defina una función que calcule el valor mayor de dos enteros 
que reciba como parámetros. Para calcularlo, utiliza un `if`.

* Define una lista con cuatro números enteros.

* Muestra en pantalla el número mayor, enter el primero y el
segundo de la lista.

* Muestra en pantalla el número mayor, entre el tercero y el cuarto de la lista.

* Muestra en pantalla el número mayor entre los dos que se han
mostrado anteriormenet.

## Ejercicio 3

Realiza las siguientes actividades en el intérprete de Python.

* Crea las siguientes variables: 
  * un diccionario de tres entradas que utilice como llave el nombre de un país al que 
te gustaría ir de viaje de paso de ecuador y como valor su capital. 
  * una lista de listas, siendo cada una de las entradas (listas) en esta lista una lista
de dos elementos: el primero el nombre de un país al que te gustaría viajar, y el segundo su capital. 

* Imprime por pantalla la longitud de la lista y del diccionario. 

* Muestra los distintos elementos de la lista y del diccionario con `print`.

* Comprueba con la sentencia print `nombre_variable` que todo lo que has hecho es correcto. 

* Fíjate -entre otras cosas- que las lista mantiene el orden que has introducido, mientras el 
diccionario -si tiene un número grande de elementos- no lo hace.

## Ejercicio 4

Escribe un programa Python que cree un fichero en el que
escriba las letras de la A a la K, una por línea, junto con
el número de orden de la letra, empezando por 1. Escribe
cada línea en el formato "A:1". Esto es, las primeras
líneas del fichero serán:

```
A:1
B:2
C:3
```

Para escribir el programa, utiliza una lista en la que estén las
letras de la A a la K.

## Ejercicio 5

Escribe un programa que lea el fichero creado en el ejercicio anterior.
Utilizando el método `split` que tienen las cadenas de texto, obtén
para cada línea la letra y el número que hay en ella.
A continuación, escribe en una línea todas las letras, separadas
por comas, y en otra todos los números, separados por puntos.
