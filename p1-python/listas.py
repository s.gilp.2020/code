#!/usr/bin/python3

lista: list = [0, 1, 2, 3, 4]
print(lista)
print(lista[1])
print(lista[0:2])
print(lista[3:])
print(lista[-1])
print(lista[:-1])
print(lista[:-2])

cadena: str = "estudiante"
print(cadena[-1])

