## Sockets UDP

Actividades:

* Frikiminutos Python: [Eliminación de fondo en fotos](../python-snippets/README.md#background).
* Frikiminutos Python: [Arte ASCII: texto](../python-snippets/README.md#asciiart).
* Explicación de la solución a la práctica 3.
* Explicación de [server.py](server.py) y [client.py](client.py)
* Captura de paquetes.
* Explicación del enunciado de la práctica 4.

**Ejercicio:** [Práctica 4. Sockets UDP y registrar SIP](ejercicios.md)

Referencias:

