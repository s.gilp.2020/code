## XML y JSON

Actividades:

* Frikiminutos Python: [Descarga de audio de YouTube](../python-snippets/README.md#youtube).
* Ejemplo de fichero XML: [chistes.xml](chistes.xml)
* Ejemplo de programa que extrae chistes de un fichero XML: [chistes.py](chistes.py)

**Ejercicio:** [Práctica 3. XML y JSON](ejercicios.md)

Referencias:

* [Módulo xml.dom.minidom](https://docs.python.org/3/library/xml.dom.minidom.html)
* [Reading (and writing) XML from Python](http://www.bioinf.org.uk/teaching/bbk/databases/dom/lecture/dom.pdf)