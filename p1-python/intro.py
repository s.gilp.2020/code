# Funciones
def a_centigrado(faren: float) -> float:
    """Convierte grados farenheit en grados centı́grados"""
    return (faren - 32) * (5.0/9)


def a_farenheit(cels: float) -> float:
    """Convierte grados centı́grados en grados farenheit"""
    return (cels * 1.8) + 32

print(a_farenheit(30))
cels: float = a_centigrado(86)
print(cels)

# Condicionales
entero: int = 3
if entero:
     print('verdadero')
else:
     print('falso')

# Strings
puntos: int = 20
categoria: str = "ganador"
print(f"Puntos del {categoria}: {puntos}")
