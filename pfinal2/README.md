# Protocolos para la transmisión de audio y video en Internet
# Proyecto final (segunda versión, junio 2023)

**Conocimientos previos necesarios:**

* Programación Python (practicas previas)
* Documentos XML y JSON (clases de teoria, prácticas previas)
* Uso de Wireshark (prácticas previas)
* Nociones de SIP (clases de teoría, prácticas previas)
* Nociones de RTP (clases de teoría, prácticas previas)

**Tiempo estimado:** 25 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-pfinal2

**Fecha de entrega (parte básica y adicional):** 28 de junio de 2023, 23:59

<!--
**Video de presentacion de detalles:** Para la práctica final de enero se grabó un video, que en gran medida es válido para esta práctica: [Video grabado el 22 de diciembre](https://urjc.sharepoint.com/:v:/s/gr_2021.2039_2039021_3AM/EaSKvs8lC1RKuzA-9Ddn52YBWlbyZxHz8O1TgTuscJOWKw?e=8zYXLa) (requiere autenticación). Pero ojo, esta práctica tiene algunos cambios: los detalles válidos son los que se incluyen en este enunciado.
-->

## Introducción

Este proyecto tiene como objetivo implementar un servicio de emisión de audio a un servicio receptor, utilizando tecnología de VoIP (SIP y RTP, fundamentalmente). Para realizar el proyecto, el alumno tendrá que realizar tres programas:

* ServidorSIP. Un programa que actuará como _registrar_ y _redirector_. Como registrar, aceptará registro de direcciones SIP de servicios y clientes. Como redirector aceptará peticiones de inicio de sesión de clientes, respondiendo con redirecciones a las direcciones de los servidores correspondientes.
* ServidorRTP. Un programa que implementará el servicio de recepción de canales de audio. El programa se registrará en el _registrar_ con una dirección SIP de servicio, y si un cliente establece una sesión (llamada) cón él (usando SIP), se preparará para recibir de él, mediante RTP, un fichero de audio.
* Cliente. Un programa que implementará el cliente. El programa comenzará registrándose en el _registrar_ con su dirección SIP de cliente. Una vez lo ha hecho, establecerá una sesión (llamada) con uno de los servicios, para enviarle el fichero de audio correspondiente. Para ello, comenzará enviando una petición de inicio de sesión a ServidorSIP, especificando la dirección de servicio y su propia dirección. ServidorSIP le contestará con una redirección a la dirección "real" del servidor, sólo si se ha registrado previamente con la misma dirección. Cliente, usando la dirección IP del servidor, establecerá una sesión RTP directamente con éste.

En este esquema, tanto ServidorRTP como Cliente funcionan como UAs SIP, aunque solo Cliente iniciará sesiones (enviará invitaciones) y tanto Cliente como ServidorRTP se registrarán con su dirección SIP. Cliente enviará datos multimedia (como paquetes RTP), que recibirá ServidorRTP.

Por lo tanto, cada ServidorRTP tendrá una "dirección de servicio", genérica, y una "dirección real", que será la que se podrá usar directamente para conectarse con él. Cada ServidorRTP registrará con ServidorSIP su dirección de servicio, indicando que corresponde con su dirección real.

La forma normal de funcionar de los tres programas será la siguiente:

* Se lanza un ServidorSIP.

* Se lanza al menos un ServidorRTP, que se registrará en el ServidorSIP con su dirección SIP de servicio, y quedará esperando invitaciones.

* Se lanza al menos un Cliente, que se registrará en el ServidorSIP con su dirección SIP, y a continuación iniciará una sesión, enviando una invitación al ServidorSIP indicando la dirección SIP de servicio de un ServidorRTP. La invitación llevará un documento SDP indicando los parámetros de la sesión.

* El ServidorSIP, actuando como _redirector_, comprobará primero que la invitación le llega de un cliente que se ha registrado previamente (usando para ello la dirección SIP del cliente), y si es así, enviará un mensaje de redirección al Cliente, indicando la dirección real que corresponde con la dirección de servicio.

* El Cliente contestará un ACK al ServidorSIP.

* El cliente enviará una invitación a la dirección real del ServidorRTP. La invitación llevará un documento SDP indicando los parámetros de la sesión.

* Si todo es correcto, el ServidorRTP responderá a la invitación aceptándola, para lo que enviará una respuesta OK al Cliente. Tras hacerlo, quedará escuchando en el puerto donde espera recibir los paquetes RTP.

* El Cliente responderá con un ACK, que enviará a ServidorRTP. A continuación, comenzará a enviar los paquetes RTP correspondientes a un fichero de audio directamente al ServidorRTP, según los parámetros especificados en el documento SDP.

* Cuando termine la transmisión del fichero, el Cliente enviará un BYE a la dirección real del ServidorRTP. Al recibirlo, este dejará de esperar paquetes RTP, y terminará la escritura del fichero recibido (haciendo un `close` sobre el descriptor de fichero correspondiente) y responderá on un OK, que enviará al Cliente.

* Al recibir el OK, el Cliente terminará.

## Objetivos del proyecto

* Realizar un sistema SIP razonablemente realista.
* Poner en práctica lo aprendido en las prácticas anteriores.
* Probar la interacción de SIP y RTP en un entorno concreto.

## Puntuación

Este documento describe el proyecto final de la asignatura (práctica final), que puede aportar hasta 2.75 puntos (sobre un total de 10) en la nota final de la asignatura. Esta puntuación se organiza como sigue:

* El funcionamiento correcto de la parte básica del proyecto supondrá que el proyecto final se considera como aprobado, pero aportará 0 puntos a la nota final de la asignatura. Tener esta parte funcionando correctamente es requisito necesario para poder aprobar la asignatura.
* El funcionamiento correcto de la parte adicional del proyecto podrá suponer hasta 2 puntos de la nota final de la asignatura.
* Además, habrá una parte a desarrollar colaborativamente, donde se probará la interacción entre prácticas de distintos alumnos, que podrá suponer hasta 0.75 puntos de la nota final de la asignatura.


## Comienzo

Con el navegador, dirígete al repositorio plantilla de este proyecto y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se exponen a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la ETSIT).

## Cliente (parte básica)

El cliente ha de ejecutarse así:

```shell
$ python3 client.py <IPServidorSIP>:<puertoServidorSIP> <dirCliente> <dirServidorRTP> <fichero>
```

donde:

* `<IPServidorSIP>` es la dirección IP de la máquina donde está ServidorSIP
* `<puertoServidorSIP>` es el puerto UDP donde está escuchando ServidorSIP
* `<dirCliente>` es la dirección SIP del cliente, con la que se va a registrar en ServidorSIP. Por ejemplo, 'sip:cliente1@clientes.net'
* `<dirServidorRTP>` es la dirección SIP de servicio del servidor con el que se va a iniciar una sesión para enviarle audio, tal y como la haya registrado el servidor en cuestión. Por ejemplo `sip:servidor1@songs.net`
* `<fichero>` será el fichero cuyo audio enviará a ServidorRTP

Por ejemplo:

```shell
$ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net miaudio.mp3
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>
```

Al arrancar, Cliente enviará un `REGISTER` a ServidorSIP, para registrarse, utilizando como dirección SIP la indicada como `<addrClient>`. La petición, por tanto, será como esta:

```
REGISTER sip:cliente1@clientes.net SIP/2.0\r\n\r\n
```

ServidorSIP deberá responder con un mensaje SIP OK.

A continuación, el cliente tratará de establecer sesión con el ServidorRTP indicado. Para ello, enviará `INVITE` al ServidorSIP, con cabecera `From`, e indicando en un documento SDP el puerto UDP desde el que enviará los paquetes RTP. Este documento SDP tendrá los siguientes elementos:

* `v`: versión, por defecto 0
* `o`: originador e identificador de sesión (la dirección SIP del Cliente y su dirección IP,
* `s`: nombre de la sesión, que será el nombre de de usuario de <addrClient> (cliente1 en el ejemplo anterior)
* `t`: tiempo que la sesión lleva activa, en nuestro caso, siempre 0
* `m`: tipo de elemento multimedia y puerto de envío y protocolo de transporte utilizados, en esta práctica `audio`, el número de puerto desde el que espear enviar paquetes RTP, ``RTP'').

Por ejemplo (suponiendo que el puerto 34543 es desde el que Cliente espera enviar los paquetes RTP):

```
INVITE sip:servidor1@songs.net SIP/2.0
From: <sip:cliente1@clientes.net>
Content-Type: application/sdp

v=0
o=sip:cliente1@clientes.net 127.0.0.1
s=cliente1
t=0
m=audio 34543 RTP
```

El Cliente quedará esperando la respuesta de ServidorSIP, que será en general un REDIRECT. Si efectivamente recibe un REDIRECT, el Cliente responderá al ServidorSIP con un ACK (incluyendo la cabecera `From`):

```
ACK sip:servidor1@songs.net SIP/2.0
From: <sip:cliente1@clientes.net>\r\n\r\n
```

A continuación, enviará de nuevo un INVITE como el anterior, pero ahora a la dirección IP y puerto indicados en la cabecera Contact del REDIRECT recibido, y usando la dirección "real" recibida en esa misma cabecera. Así, si la cabecera Contact recibida en el REDIRECT fue:

```
Contact: sip:servidor1@192.168.10.16:53001
```

El INVITE se enviará a la dirección IP 192.168.10.16, puerto 5001, y será:

```
INVITE sip:servidor1@192.168.10.16:53001 SIP/2.0
From: <sip:cliente1@clientes.net>
Content-Type: application/sdp

v=0
o=sip:cliente1@clientes.net 127.0.0.1
s=cliente1
t=0
m=audio 34543 RTP
```

El cliente quedará esperando el OK de ServidorRTP, y cuando lo reciba, enviará un ACK a ServidorSIP:

```
ACK sip:heyjude@@192.168.10.16:53001 SIP/2.0
From: <sip:cliente1@clientes.net>\r\n\r\n
```

Y comenzará a enviar los paquetes RTP desde su puerto 34543 al puerto indicado en el documento SDP que ha debido venir con el `OK`que ha recibido.

Cuando se termine el fichero de audio, Cliente enviará un BYE:

```
BYE sip:servidor1@s@192.168.10.16:53001 SIP/2.0\r\n\r\n
```

y cuando reciba el OK correspondiente, terminará.

Para enviar los paquetes RTP al servidor, se puede usar un código similar al que se puede ver en el programa `send_rtp.py` que hemos incluido en el repositorio plantilla de la práctica final. Revisa el código de este programa ejemplo con cuidado, los comentarios deberían aclarar todos sus detalles.


## ServidorRTP (parte básica)

El servidor RTP ha de ejecutarse así:

```shell
$ python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio>
```

donde:

* `<IPServidorSIP>` es la dirección IP de la máquina donde está ServidorSIP
* `<puertoServidorSIP>` es el puerto UDP donde está escuchando ServidorSIP
* `<servicio>` es el usuario que se uará como parte de usuario tanto en la dirección de servicio SIP como en la dirección real SIP para este servidor.

Por ejemplo:

```shell
$ python3 serverrtp.py 127.0.0.1:6001 servidor1
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>
```

Al arrancar, ServidorRTP enviará un `REGISTER` a ServidorSIP, para registrarse, utilizando como dirección SIP el nombre de usuario que se indicó como `<servicio>`, concatenado con `@songs.net`. Por ejemplo, si el usuario es `servidor1`, la dirección será `sip:servidor1@songs.net`. La petición, por tanto, será como esta:

```
REGISTER sip:servidor1@songs.net SIP/2.0\r\n\r\n
```

ServidorSIP deberá responder con un mensaje SIP OK (incluyendo una cabecera `From`).

A continuación, ServidorRTP quedará esperando peticiones `INVITE` de los clientes. Cuando las reciba correctamente, se preparará para recibir paquetes RTP del cliente, y responderá con un OK (siendo el puerto 20354 el puerto donde está esperando recibir los paquetes RTP):

```
SIP/2.0 200 OK
From: <sip:servidor1@songs.net>
Content-Type: application/sdp

v=0
o=sip:cliente1@clientes.net 127.0.0.1
s=cliente1
t=0
m=audio 20354 RTP
```

A partir de este momento, cuando reciba paquetes RTP los irá almacenando en un fichero, que tendrá siempre el nombre `recibido.mp3`

Quedará tembién esperando paquetes SIP. Si recibe un paquete `ACK`, no hará nada en particular (indica que los paquetes RTP vienen a continuación). Cuando reciba un paquete `BYE` del cliente, terminará la recepción, cerrando el fichero, y responderá con un OK (incluyendo la cabecera `From`):

```
SIP/2.0 200 OK
From: <sip:servidor1@songs.net>\r\n\r\n
```

A partir de ese momento, quedará esperando una nueva petición `INVITE`  de otro cliente.

Para recibir los paquetes RTP se puede usar un código similar al que se puede ver en el programa `recv_rtp.py` que hemos incluido en el repositorio plantilla de la práctica final. Ese código genera un fichero con los datos recibidos en los paquetes RTP, pero no necesariamente un fichero MP3 correcto. No hace falta que el fichero generado sea un fichero MP3 correcto, basta con que se escriban en él los datos recibidos en el `payload` de los
paquetes RTP, en cualquier orden (que es lo que hace `recv_rtp.py`). Revisa el código de este programa ejemplo con cuidado, los comentarios deberían aclarar todos sus detalles.


## ServidorSIP (parte básica)

El servidor SIP ha de ejecutarse así:

```shell
$ python3 serversip.py <puerto>
```

donde:

* `<puerto>` será el puerto UDP donde recibirá peticiones SIP tanto de clientes como de servidores RTP.

Por ejemplo:

```shell
$ python3 serversip.py 6001
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverrtp.py <port>
```

Al arrancar, ServidorSIP funcionará como _registrar_ y como _redirector_. Por lo tanto, aceptará varias peticiones SIP:

* `REGISTER`: Registrará en un diccionario que va a mantener con todas las direcciones registradas. El diccionario tendrá como claves las direcciones SIP que se registren, y como valor para cada clave las tuplas <IP>:<puerto> (dirección IP y puerto de origen del paquete de registro). En esas direcciones será donde los ServidorRTPs estarán esperando mensajes SIP. Los datos de este diccionario serán respaldados en el fichero JSON llamado `registrar.json`, con la misma estructura que el diccionario. Este fichero se actualizará cada vez que haya algún cambo en los datos de registro. Cuando arranque el servidor comprobará si existe un fichero con ese nombre, y si es así lo tratará de leer para inicializar el diccionario de registro.

* `INVITE`, `ACK`: Cuando los reciba, actuara como _redirector_. Pero solo actuará como tal si el emisor de la petición SIP se ha registrado (usando un `REGISTER`) previamente. Si no es así, responderá con un mensaje  Por lo tanto, cuando reciba `INVITE` responderá con un paquete `REDIRECT`, indicando al dirección real que corresponde con la dirección de servicio que venía en el `INVITE`. Cuando reciba un `ACK`, no hará nada.

Por ejemplo, si se recibe un `REGISTER` ha sido enviado desde un ServidorRTP que está en la máquina 192.168.10.16, escuchando en el puerto 53001, y tiene un contenido como el del paquete `REGISTER` que envía el ServidorRTP en el ejemplo anterior, el ServidorSIP guardará en su diccionario de direcciones registradas una nueva entrada con la clave `sip:server1@songs.net`, y el valor `sip:server1@192.168.10.16:53001` (dirección "real" que corresponde a esa dirección de servicio). A continuación responderá con un paquete OK:

```
SIP/2.0 200 OK\r\n\r\n
```

Por ejemplo, si se recibe un `INVITE` como primer paquete que envió un Cliente en el ejemplo que se puso más arriba, la respuesta será una redirección `302 Moved Temporarily`, indicando en la cabecera `Contact` la dirección "real" con la que tendrá que contactar el cliente:

```
SIP/2.0 302 Moved Temporarily\r\n
Contact: sip:heyjude@192.168.10.16:5001\r\n
\r\n
```


## Históricos  (parte básica)

Todos los programas (Cliente, ServidorRTP y ServidorSIP) escribirán en su salida estándar (`stdout`) mensajes históricos (_log_) cuando ocurran ciertos eventos, siempre en el siguiente formato (nótese el punto final):

```
YYYYMMDDHHMMSS Evento.
```

donde `YYYYMMDDHHMMSS` es el tiempo, en ese formato, y `Evento` es:

* Cuando un programa empieza: `Starting...`

* Para cada envío/recepción de una petición o una respuesta SIP: `SIP to/from <ip>:<port> <line>.`, siendo `<ip>` y `<port>` la dirección IP y el puerto de destino o de origen de la petición o la respuesta SIP.

* Cuando un ServidorRTP se prepare para recibir paquetes RTP en un puerto: `RTP ready <port>`, siendo `<port>` ese puerto.

* Cuando un ServidorRTP haya terminado de recibir paquetes RTP en un puerto: `RTP all content received <port>`, siendo `<port>` ese puerto.

* Cuando un Cliente empiece a enviar paquetes RTP a una cierta dirección IP y puerto: `RTP to <ip>:<port>`, siendo `<ip>` y `<port>` esa dirección y ese puerto.

Nótese, por tanto, que cada línea ha de contener la fecha en el formato indicado, luego un espacio en blanco y luego el texto del evento. Por ejemplo:

```
20211128152045 Starting...
20211128153012 SIP to 127.0.0.1:5555: REGISTER sip:heyjude@signasong.net SIP/2.0.
20211128153017 SIP from 127.0.0.1:5555: SIP/2.0 200 OK.
20211128153036 RTP to 127.0.0.1:4444.
```

## Capturas (parte básica)

Han de realizarse las siguientes capturas con Wireshark, que se entregarán con el resto del proyecto en el repositorio correspondiente:

* `capture.libpcap`: Paquetes SIP y RTP intercambiados entre todos los programas en una transferencia del audio del fichero `cancion.mp3` desde un Cliente a un ServerRTP. Se realizará lanzando, en este orden (en consolas diferentes) los siguientes programas con los correspondientes argumentos:

```shell
$ python3 serversip.py 6001
$ python3 serverrtp.py 127.0.0.1:6001 servidor1
$ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net cancion.mp3
```

Además del fichero con la captura, se incluirá también el fichero almacenado por el servidor (`recibido.mp3`).

* `capture2.libpcap`: Paquetes SIP y RTP intercambiados entre todos los programas en una transferencia de un audio desde dos clientes a un ServerRTP, en secuencia. Se realizará lanzando, en este orden (en consolas diferentes) los siguientes programas con los correspondientes argumentos:

```shell
$ python3 serversip.py 6001
$ python3 serverrtp.py 127.0.0.1:6001 servidor1
$ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net cancion.mp3
$ mv recibido.mp3 recibido1.mp3
$ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net cancion.mp3
$ mv recibido.mp3 recibido2.mp3
```

Además del fichero con la captura, se incluirán también los ficheros `recibido1.mp3` y `recibido2.mp3`.

## Peticiones concurrentes (parte adicional)

ServidorRTP acepta peticiones concurrentes de varios clientes. Esto es, recibe paquetes RTP simulatáneamente de dos o más clientes. en este caso, almacenará la secuencia RTP recibida de cada cliente en un fichero distinto (mencionar en el fichero `README.md` de entrega cuál es el nombre de esos ficheros).

## Configuración XML (parte adicional)

ServidorRTP utiliza un fichero XML para almacenar datos sobre direcciones de servicio SIP y nombres de fichero, de forma que un solo servidor puede servir un conjunto de direcciones, cada una correspondiente con un fichero diferente: si un cliente envía un fichero mediante RTP a uan dirección, almacena los paquetes en el fichero correspondiente, según lo indicado en el fichero XML. Mencionar en el fichero `README.md` de entrega el nombre del fichero XML, y su estructura.

## Recepción por cliente (parte adicional)

Cliente recibe también, en cada sesión que establezca con ServidorRTP, un stream de audio RTP. ServidorRTP enviará, por tanto, un fichero a la vez qu erecibe el que le envía Cliente.

## Cabecera de tamaño (parte adicional)

Se incluirá en los paquetes SIP la cabecera de tamaño del cuerpo del paquete, cuando haya cuerpo.

## Tiempo de expiracion (parte adicional)

Tiempo de expiración de las direcciones SIP registradas, utilizando la cabecera correspondiente en `REGISTER`. Los clientes se registrarán por 20 segundos. Los ServidorRTP se registrarán por 62 segundos, y al cabo de 30 segundos renovará el registro enviando un nuevo `REGISTER`, de nuevo con periodo de validez de 62 segundos (así el registro se mantendrá aunque se perdiera un paquete `REGISTER`).

## Gestión de errores (parte adicional)

Gestión de errores, enviando las correspondientes respuestas en lugar de `200 OK`.

## Entrega de la práctica

La práctica se entregará en el repositorio del alumno en el GitLab de la ETSIT, que habrá sido derivado (haciendo "fork", como se indica al comienzo de este enunciado) del repositorio plantilla del proyecto final.

El repositorio deberá incluir los ficheros que había en el repositorio plantilla, más:

* Ficheros con programas principales `serversip.py`, `serverrtp.py` y `client.py`.
* Capturas indicadas en el apartado "Capturas", más arriba: `capture.libpcap` y `capture2.libcap`
* Ficheros de audio grabados con las capturas: `recibido.mp3`, `recibido1.mp3` y `recibido2.mp3`

* Fichero de entrega, `entrega.md`, en formato Markdown, con la siguiente información:

  - Sección sobre la parte básica (`## Parte básica`). Incluirá cualquier comentario relevante sobre la parte básica de la práctica, incluyendo cualquier aspecto que no funcione como se indica en el enunciado.
  - Sección sobre la parte adicional (`## Parte adicional`). Incluirá cualquier comentario relevante sobre la parte adicional de la práctica, incluyendo fundamentalmente el listado de las opciones que se han realizado (una por línea, comenzando por `* `, para que queden en un listado, y con el mismo contenido en esa línea que el nombre del apartado correspondiente paa esa opción). Por ejemplo:

```markdown
* Configuración XML

    Explicación
  
* Cabecera de tamaño

    Explicación
```

  Además de este listado incluirá una explicación (ver el ejemplo anterior) sobre cómo probar esa parte opcional, indicando al menos qué programas hay que lanzar, con qué argumentos, y cómo se puede comprobar que funciona el apartado en cuestion.

  Si se ha realizado cualquier parte adicional que no venga indicada específicamente en el enunciado (por ejemplo, se han implementado peticiones SIP adicionales, o cabeceras SIP adicionales), puede indicarse en este listado como `Otra:`, seguido de una descripción de la mejora (una entrada `Otra` por cada mejora).

  - Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Es importante comprobar que este documento se ve correctamente desde la interfaz eb de GitLab (eso es, que el marcado Markdown se interpreta correctamente)


## Preguntas y respuestas

Ver las preguntas y respuestas del proyecto final (convocatoria de enero).
